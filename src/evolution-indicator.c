/*
 * Copyright (C) 2009 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gprintf.h>

#include <gconf/gconf.h>

#include <canberra.h>
#include <libnotify/notify.h>

#include <time.h>
#include <string.h>

#include <e-util/e-config.h>
#include <mail/em-utils.h>
#include <mail/em-event.h>
#include <mail/em-folder-tree-model.h>
#include <shell/es-event.h>
#include <shell/e-shell-view.h>

#include <libindicate/server.h>
#include <libindicate/indicator.h>
#include <libindicate/indicator-messages.h>

#include <unity.h>

#include "xutils.h"

#define CONF_DIR          "/apps/evolution/eplugin/evolution_indicator"
#define ONLY_INBOX        CONF_DIR"/only_inbox"
#define PLAY_SOUND        CONF_DIR"/play_sound"
#define SHOW_BUBBLE       CONF_DIR"/show_bubble"
#define SHOW_NEW_IN_PANEL CONF_DIR"/show_new_messages_in_panel"

#define ACCOUNT_DIR "/apps/evolution/mail"
#define ACCOUNTS    ACCOUNT_DIR"/accounts"

#define EVOLUTION_DESKTOP_FILE "/usr/share/applications/evolution.desktop"

#define UNREAD_DATA "unread"

#define  USER_BLACKLIST_DIR      "indicators/messages/applications-blacklist"
#define  USER_BLACKLIST_FILENAME "evolution"

static GStaticMutex  mlock       = G_STATIC_MUTEX_INIT;
static GConfClient  *client      = NULL;

static IndicateServer *server     = NULL;
static GSList         *indicators = NULL;
static gint            n_accounts = 0;

static UnityLauncherEntry       *launcher = NULL;

static NotifyNotification *notification   = NULL;
static ca_context         *canberra_cxt   = NULL;
static ca_proplist        *canberra_props = NULL;

static gboolean      is_active   = TRUE;
static gboolean      only_inbox  = TRUE;
static gboolean      play_sound  = TRUE;
static gboolean      show_bubble = TRUE;
static gboolean      show_count  = FALSE;

static guint         only_inbox_id  = 0;
static guint         play_sound_id  = 0;
static guint         show_bubble_id = 0;
static guint         show_count_id  = 0;
static guint         accounts_id    = 0;

static gint message_count = 0;

void org_gnome_mail_new_notify    (EPlugin *ep, EMEventTargetFolder *t);
void org_gnome_mail_read_notify   (EPlugin *ep, EMEventTargetMessage *t);

int         e_plugin_lib_enable               (EPlugin *ep, int enable);
GtkWidget * e_plugin_lib_get_configure_widget (EPlugin *ep);

static void show_evolution (gpointer arg0, guint timestamp, gpointer arg1);

static void show_evolution_in_indicator_applet (void);
static void hide_evolution_in_indicator_applet (void);

typedef struct {
  gchar *url;
  gchar *name;
  gchar *parent;
  gchar *last_parent;

  gint reap_type;

  gboolean enabled;

} ParserData;

enum {
  REAP_NONE = 0,
  REAP_URL,
  REAP_NAME
};

static GtkWidget *
get_cfg_widget (void)
{
	GtkWidget *vbox;

	vbox = gtk_vbox_new (FALSE, 6);

	gtk_widget_show (vbox);

	return vbox;
}

static gboolean
evolution_is_focused (void)
{
#define MAIL_ICON "evolution-mail"
  static GdkScreen *screen = NULL;
  static GdkWindow *root = NULL;
  gboolean          res = FALSE;
  Window            xwindow;
  GList *list;
  EShell *evo_shell;
  EShellWindow *shell_window;
  
  /* Try and do a match through X, by grabbing the current active window and
     checking to see if it's an evolution window */
  if (screen == NULL || root == NULL)
  {
    screen = gdk_screen_get_default ();
    root = gdk_screen_get_root_window (screen);
  }

  xwindow = None;
#if GTK_CHECK_VERSION(2, 91, 0)
  res = _wnck_get_window (GDK_WINDOW_XID (root), 
#else
  res = _wnck_get_window (GDK_WINDOW_XWINDOW (root),
#endif
                    gdk_x11_get_xatom_by_name ("_NET_ACTIVE_WINDOW"),
                    &xwindow);
  if (res)
  {
    gchar *res_class = NULL;
    gchar *res_name = NULL;

    _wnck_get_wmclass (xwindow, &res_class, &res_name);

    if (!g_strcmp0 (res_name, "evolution"))
    {
      g_free (res_class);
      g_free (res_name);
      return TRUE;
    }
    g_free (res_class);
    g_free (res_name);
  }

  evo_shell = e_shell_get_default ();
  list = e_shell_get_watched_windows (evo_shell);

  /* Find the first EShellWindow in the list. */
  while (list != NULL && !E_IS_SHELL_WINDOW (list->data))
    list = g_list_next (list);

  g_return_if_fail (list != NULL);

  /* Get the shell window. */
  shell_window = E_SHELL_WINDOW (list->data);

  if (GTK_IS_WINDOW (shell_window))
  {
      if (gtk_window_is_active (GTK_WINDOW (shell_window)))
      {
        return TRUE;
      }
  }
  return FALSE;
}

static gint
get_indicator_unread_count (IndicateIndicator *indicator)
{
  return GPOINTER_TO_INT (g_object_get_data (G_OBJECT (indicator),
                                             UNREAD_DATA));
}

static void
set_indicator_unread_count (IndicateIndicator *indicator, gint count)
{
  gchar *count_str;

  count_str = g_strdup_printf ("%d", count);
  indicate_indicator_set_property (indicator,
                                   INDICATE_INDICATOR_MESSAGES_PROP_COUNT,
                                   count_str);
  g_object_set_data (G_OBJECT (indicator),
                     UNREAD_DATA,
                     GINT_TO_POINTER (count));

  g_free (count_str);
}

void
org_gnome_mail_new_notify (EPlugin *ep, EMEventTargetFolder *t)
{
  gchar *url;

  g_return_if_fail (t != NULL);

  url = camel_service_get_url (CAMEL_SERVICE (t->store));

  if (!t->new)
		return;

  if (only_inbox && !(t->is_inbox))
  {
    g_debug ("EI: %s is not an inbox", url);
    return;
  }

  if (evolution_is_focused ())
{
    g_debug ("EI: Evolution is focused");
    return;
  }

	g_static_mutex_lock (&mlock);

  g_debug ("EI:mail_new_notify: %s", url);

  message_count += t->new;
 
  if (show_count)
  {
    IndicateIndicator *indicator = NULL;
    GSList *i;

    for (i = indicators; i; i = i->next)
      {
        IndicateIndicator *indi = i->data;

        if (g_strstr_len (url,
                          -1,
                          indicate_indicator_get_property (indi, "url")))
          {
            indicator = indi;
            break;
          }
      }
    if (indicator)
      {
        gint count;
          
        count = get_indicator_unread_count (indicator);
        set_indicator_unread_count (indicator, count + t->new);

        indicate_indicator_set_property (indicator,
                                         INDICATE_INDICATOR_MESSAGES_PROP_ATTENTION,
                                         "true");
      }
    else
      {
        g_warning ("EI: Unable to find account that matches %s", url);
      }
    }

  update_unity_launcher_count ();

  /* Show bubble */
  if (show_bubble)
  {
    GError *error = NULL;
    const gchar *trans;
    gchar *title;

    if (notification == NULL)
    {
      notification = notify_notification_new (" ", " ", "mail-unread");
    }

    trans = g_dngettext (PACKAGE, 
                         "%d New Message", 
                         "%d New Messages",
                         message_count);
    
    title = g_strdup_printf (trans, message_count);
    
    notify_notification_update (notification,
                                title, NULL, "notification-message-email");

    /* This plays a libcanberra sound */
    if (play_sound)
      notify_notification_set_hint_string (notification, 
                                           "sound-themed",
                                           "message-new-email");

    notify_notification_show (notification, &error);

    if (error)
    {
      g_warning ("EI: Could not update: %s", error->message);
      g_error_free (error);
    }
  }

  /* Check if we need to play the sound differently */
  if (!show_bubble && play_sound)
  {
    gint ret;
    
    g_debug ("EI: No bubbles enabled so playing sound ourselves");

    ret = ca_context_play (canberra_cxt, 0,
                           CA_PROP_EVENT_ID, "message-new-email",
                           CA_PROP_MEDIA_LANGUAGE, "en_EN",
                           CA_PROP_CANBERRA_CACHE_CONTROL, "permanent",
                           NULL);

    g_warning ("EI: Unable to play sound: %s\n", ca_strerror (ret));
  }

  g_static_mutex_unlock (&mlock);
}

void
org_gnome_mail_read_notify (EPlugin *ep, EMEventTargetMessage *t)
{
  g_return_if_fail (t != NULL);

  GSList *i;

	g_static_mutex_lock (&mlock);

  g_debug ("EI: mail_read_notify");

  for (i = indicators; i; i = i->next)
    {
      IndicateIndicator *indicator = i->data;

      set_indicator_unread_count (indicator, 0);
      indicate_indicator_set_property (indicator,
                                       INDICATE_INDICATOR_MESSAGES_PROP_ATTENTION,
                                       "false");

      g_debug ("EI: Setting %s to 0 unread messages",
               indicate_indicator_get_property (indicator, "name"));

    }
  message_count = 0;

  update_unity_launcher_count ();

  g_static_mutex_unlock (&mlock);
}

void
update_unity_launcher_count ()
{
  GSList *i;
  int count = 0;

  g_debug ("EI: update_unity_launcher_count");

  for (i = indicators; i; i = i->next)
  {
    IndicateIndicator *indicator = i->data;

    count = count + get_indicator_unread_count (indicator);

    g_debug ("EI: Setting count to %d unread messages", count);

  }
  unity_launcher_entry_set_count (launcher, count);
  if (count > 0)
  {
    unity_launcher_entry_set_count_visible (launcher, TRUE);
  } else {
    unity_launcher_entry_set_count_visible (launcher, FALSE);
  }
}

/*
 * GCONF CLIENTS
 */
static void
only_inbox_changed (GConfClient *gclient, 
                    guint        id,
                    GConfEntry  *entry,
                    gpointer     data)
{
  GConfValue *value;

  value = entry->value;

  only_inbox = gconf_value_get_bool (value);

  g_debug ("EI: Only show Inbox %s", only_inbox ? "true" : "false");
}

static void
play_sound_changed (GConfClient *gclient, 
                    guint        id,
                    GConfEntry  *entry,
                    gpointer     data)
{
  GConfValue *value;

  value = entry->value;

  play_sound = gconf_value_get_bool (value);

  g_debug ("EI: Play Sounds %s", play_sound ? "true" : "false");
}

static void
show_new_in_panel_changed (GConfClient *gclient, 
                           guint        id,
                           GConfEntry  *entry,
                           gpointer     data)
{
  GConfValue *value;

  value = entry->value;

  show_count = gconf_value_get_bool (value);
 
  if (show_count)
    {
      indicate_server_show (server);
      show_evolution_in_indicator_applet ();
    }
  else
    {
      indicate_server_hide (server);
      hide_evolution_in_indicator_applet ();
    }

  g_debug ("EI: Messages in panel %s", 
           show_count ? "true" : "false");
}

static void
show_bubble_changed (GConfClient *gclient, 
                     guint        id,
                     GConfEntry  *entry,
                     gpointer     data)
{
  GConfValue *value;

  value = entry->value;

  show_bubble = gconf_value_get_bool (value);

  g_debug ("EI: Show Bubbles %s", show_bubble ? "true" : "false");
}

static IndicateIndicator *
find_indicator_for_url (GSList *indicator_list, const gchar *url)
{
  GSList *i;

  for (i = indicator_list; i; i = i->next)
    {
      IndicateIndicator *indicator = i->data;

      if (g_strcmp0 (indicate_indicator_get_property (indicator, "url"), url)
          == 0)
        return indicator;
    }
  return NULL;
}

static IndicateIndicator *
create_indicator (const gchar *url, const gchar *name)
{
  IndicateIndicator *indicator;

  indicator = indicate_indicator_new ();
  indicate_indicator_set_property (indicator,
                                   INDICATE_INDICATOR_MESSAGES_PROP_NAME,
                                   name);
  indicate_indicator_set_property (indicator,
                                   "url",
                                   url);
  set_indicator_unread_count (indicator, 0);
  indicate_indicator_show (indicator);

  /* FIXME: I need to find a way to show a mailbox individually */
  g_signal_connect (indicator, "user-display",
                    G_CALLBACK (show_evolution), NULL);

  g_debug ("EI: New Indicator: %s %s", name, url);

  return indicator;
}

static void
start_element_handler (GMarkupParseContext *context,
                       const gchar         *element_name,
                       const gchar        **attribute_names,
                       const gchar        **attribute_values,
                       gpointer             user_data,
                       GError             **error)
{
  ParserData *data = (ParserData *)user_data;

  if (g_strcmp0 (element_name, "account") == 0)
    {
      gint i = 0;

      while (attribute_names[i] != NULL)
        {
          if (g_strcmp0 (attribute_names[i], "name") == 0)
            {
              data->name = g_strdup (attribute_values[i]);
            }
          else if (g_strcmp0 (attribute_names[i], "enabled") == 0)
            {
              if (g_strcmp0 (attribute_values[i], "false") == 0)
                data->enabled = FALSE;
            }
          i++;
        }
    }
  else if (g_strcmp0 (element_name, "url") == 0)
    data->reap_type = REAP_URL;
  else
    data->reap_type = REAP_NONE;

  if (data->last_parent)
    g_free (data->last_parent);
  
  data->last_parent = data->parent;
  data->parent = g_strdup (element_name);
}

static void
text_handler (GMarkupParseContext *context,
              const gchar         *text,
              gsize                text_len,
              gpointer             user_data,
              GError             **error)
{
  ParserData *data = (ParserData *)user_data;

  if (!data->url
      && data->reap_type == REAP_URL
      && g_strcmp0(data->last_parent, "source") == 0)
    {
      gchar **tokens;

      tokens = g_strsplit (text, ";", 2);

      data->url = g_strdup (tokens[0]);

      /* Accounts with no configured way to receive mail will not have a URL */
      if (!data->url)
        data->enabled = FALSE;

      g_strfreev (tokens);
    }
}

static void
update_accounts (void)
{
  GSList   *accounts;
  GError   *error = NULL;
  gint      i = 1;
  GTimeVal  timeval = { 0 };
  
  g_get_current_time (&timeval);
  accounts = gconf_client_get_list (client,
                                    ACCOUNTS,
                                    GCONF_VALUE_STRING,
                                    &error);
  if (accounts == NULL || error)
    {
      g_warning ("EI: Unable to determine number of accounts, "
                 "defaulting to '1' (%s)",
                 error ? error->message : "unknown");
      if (error)
        g_error_free (error);
      
      /* We could have this as 0 too, as it won't effect anything. It just
       * seems to make more sense to have it default at 1
       */
      n_accounts = 1;
    }
  else
    {
      GSList *old_list;
      GSList *a;
      static GMarkupParser parser = {
          start_element_handler,
          NULL,
          text_handler,
          NULL,
          NULL
      };

      old_list = indicators;
      indicators = NULL;
      
      for (a = accounts; a; a = a->next)
        {
          gchar               *account_info = a->data;
          GMarkupParseContext *context;
          ParserData           data = { NULL, NULL, NULL, NULL, 0, TRUE };
          IndicateIndicator   *indicator;
                                        
          /* Parse account XML to get some useful details about the account */
          context = g_markup_parse_context_new (&parser, 0, &data, NULL);
          g_markup_parse_context_parse (context,
                                        account_info,
                                        strlen (account_info),
                                        NULL);

          /* Check to see account already exists and, if not, create it */
          indicator = find_indicator_for_url (indicators, data.url);
          if (indicator)
            {
              old_list = g_slist_remove (old_list, indicator);
              indicators = g_slist_append (indicators, indicator);
            }
          else
            {
              if (data.url && g_str_has_prefix (data.url, "pop:"))
                {
                  indicator = create_indicator ("pop:",
                                                g_dgettext (EVO_I18N_DOMAIN,
                                                            "Inbox"));
                }
              else
                {
                  indicator = create_indicator (data.url, data.name);
                }
              indicators = g_slist_append (indicators, indicator);

              g_debug ("EI: New account: %s (%s)", data.name, data.url);
            }

          if (!data.enabled)
            indicate_indicator_hide (indicator);

          /* Fake a time */
          g_time_val_add (&timeval, -1000000 * 60 * i);
          indicate_indicator_set_property_time (indicator,
                                     INDICATE_INDICATOR_MESSAGES_PROP_TIME,
                                                &timeval);

          i++;

          /* Clean up */
          g_free (data.url);
          g_free (data.name);
          g_free (data.parent);
          g_free (data.last_parent);
          data.reap_type = REAP_NONE;
          data.enabled = TRUE;
          
          g_markup_parse_context_free (context);
        }

      g_slist_foreach (old_list, (GFunc)g_object_unref, NULL);
      g_slist_free (old_list);

      n_accounts = g_slist_length (accounts);
      g_slist_free (accounts);
    }

  g_debug ("EI: Number of email accounts: %d", n_accounts);
}

static void
on_accounts_changed (GConfClient *gclient,
                     guint        id,
                     GConfEntry  *entry,
                     gpointer     data)
{
  update_accounts ();
}

#define EVO_CONTACTS_CMD  "evolution -c contacts"
#define EVO_COMPOSE_CMD   "evolution mailto:"

static void
command_item_activate (DbusmenuMenuitem * mi, guint timestamp, gpointer user_data)
{
	gchar * command = (gchar *)user_data;
	if (!g_spawn_command_line_async(command, NULL)) {
		g_warning("EI: Unable to execute command '%s'", command);
	}
	return;
}

int
e_plugin_lib_enable (EPlugin *ep, int enable)
{
  is_active = enable;

  if (is_active)
  {
    if (notification == NULL)
      notify_init ("evolution-indicator");

    if (canberra_cxt == NULL)
    {
      gint ret;

      ret = ca_context_create (&canberra_cxt);
      if (ret)
      {
        g_warning ("EI: Canberra Init Error: %s", ca_strerror (ret));
      }
      else
      {
        ret = ca_context_change_props (canberra_cxt, 
                                       CA_PROP_APPLICATION_NAME, "evolution-indicator", 
                                       CA_PROP_APPLICATION_ID, "org.freedesktop.evolution-indicator",
                                       CA_PROP_WINDOW_X11_XID, ":0",
                                       NULL);
        if (ret)
        {
          g_warning ("EI: Unable to set props: %s\n", ca_strerror (ret));
        }
      }
    }

    server = indicate_server_ref_default ();
    indicate_server_set_type (server, "message");
    indicate_server_set_desktop_file (server, EVOLUTION_DESKTOP_FILE);
    g_signal_connect (server, "server-display",
                      G_CALLBACK (show_evolution), NULL);

	DbusmenuServer * menu_server = dbusmenu_server_new("/messaging/commands");
	DbusmenuMenuitem * root = dbusmenu_menuitem_new();

	DbusmenuMenuitem * mi = dbusmenu_menuitem_new();
	dbusmenu_menuitem_property_set(mi, DBUSMENU_MENUITEM_PROP_LABEL, _("Compose New Message"));
	g_signal_connect(G_OBJECT(mi), DBUSMENU_MENUITEM_SIGNAL_ITEM_ACTIVATED, G_CALLBACK(command_item_activate), EVO_COMPOSE_CMD);
	dbusmenu_menuitem_child_append(root, mi);

	mi = dbusmenu_menuitem_new();
	dbusmenu_menuitem_property_set(mi, DBUSMENU_MENUITEM_PROP_LABEL, _("Contacts"));
	g_signal_connect(G_OBJECT(mi), DBUSMENU_MENUITEM_SIGNAL_ITEM_ACTIVATED, G_CALLBACK(command_item_activate), EVO_CONTACTS_CMD);
	dbusmenu_menuitem_child_append(root, mi);

	dbusmenu_server_set_root(menu_server, root);
	indicate_server_set_menu(server, menu_server);

    launcher = unity_launcher_entry_get_for_desktop_file (EVOLUTION_DESKTOP_FILE);

    client = gconf_client_get_default ();
    gconf_client_add_dir (client, CONF_DIR, GCONF_CLIENT_PRELOAD_NONE, NULL);

    only_inbox = gconf_client_get_bool (client, ONLY_INBOX, NULL);
    only_inbox_id = gconf_client_notify_add (client, ONLY_INBOX, 
                             only_inbox_changed, NULL, NULL, NULL);

    play_sound = gconf_client_get_bool (client, PLAY_SOUND, NULL);
    play_sound_id = gconf_client_notify_add (client, PLAY_SOUND, 
                             play_sound_changed, NULL, NULL, NULL);

    play_sound = gconf_client_get_bool (client, PLAY_SOUND, NULL);
    play_sound_id = gconf_client_notify_add (client, PLAY_SOUND, 
                             play_sound_changed, NULL, NULL, NULL);

    show_bubble = gconf_client_get_bool (client, SHOW_BUBBLE, NULL);
    show_bubble_id = gconf_client_notify_add (client, SHOW_BUBBLE, 
                             show_bubble_changed, NULL, NULL, NULL);

    show_count = gconf_client_get_bool (client, 
                                        SHOW_NEW_IN_PANEL, 
                                        NULL);
    show_count_id = gconf_client_notify_add (client, SHOW_NEW_IN_PANEL, 
                             show_new_in_panel_changed, NULL, NULL, NULL);

    gconf_client_add_dir (client, ACCOUNT_DIR,GCONF_CLIENT_PRELOAD_NONE, NULL);
    update_accounts ();
    accounts_id = gconf_client_notify_add (client, ACCOUNTS,
                                           on_accounts_changed, NULL,
                                           NULL, NULL);

    if (show_count)
    {
      indicate_server_show (server);
      show_evolution_in_indicator_applet ();
    }
    else
      {
        indicate_server_hide (server);
        hide_evolution_in_indicator_applet ();
      }
  }
  else
  {
    gconf_client_notify_remove (client, only_inbox_id);
    gconf_client_notify_remove (client, play_sound_id);
    gconf_client_notify_remove (client, show_bubble_id);
    gconf_client_notify_remove (client, show_count_id);
    gconf_client_notify_remove (client, accounts_id);

    g_object_unref (client);
    client = NULL;
    
    /* Free indicators */
    g_slist_foreach (indicators, (GFunc)g_object_unref, NULL);
    g_slist_free (indicators);
    indicators = NULL;

    /* Free server */
    indicate_server_hide (server);
    g_object_unref (server);
    server = NULL;

    /* Remove evolution from indicator menu */
    hide_evolution_in_indicator_applet ();
    
    g_debug ("EI: Disabled");
  }

	return 0;
}

GtkWidget *
e_plugin_lib_get_configure_widget (EPlugin *ep)
{
  g_debug ("EI: Get Configure Widget");
	return get_cfg_widget ();
}

static void
on_combo_changed (GtkComboBox *combo, gpointer null)
{
  gconf_client_set_bool (client,
                         ONLY_INBOX,
                         gtk_combo_box_get_active (combo) ? FALSE : TRUE,
                         NULL);
}

static void
on_sound_toggled (GtkToggleButton *button, gpointer null)
{
  gconf_client_set_bool (client, PLAY_SOUND, 
                         gtk_toggle_button_get_active (button), NULL);
}

static void
on_bubble_toggled (GtkToggleButton *button, gpointer null)
{
  gconf_client_set_bool (client, SHOW_BUBBLE,
                         gtk_toggle_button_get_active (button), NULL);
}

static void
on_show_panel_toggled (GtkToggleButton *button, gpointer null)
{
  gconf_client_set_bool (client, SHOW_NEW_IN_PANEL,
                         gtk_toggle_button_get_active (button), NULL);
}

GtkWidget *
org_gnome_get_prefs (EPlugin *ep, EConfigHookItemFactoryData *data)
{
  GtkWidget *vbox, *frame=NULL, *check;
  
  g_debug ("EI: MAIL PREFS");

	if (data->old)
    return data->old;

  frame = data->parent;
  while (!GTK_IS_FRAME (frame))
  {
    frame = gtk_widget_get_parent (frame);

    if (GTK_IS_WINDOW (frame) || !GTK_IS_WIDGET (frame))
      break;
  }

  if (GTK_IS_FRAME (frame))
  {
    GtkWidget *frame, *box, *label1, *label2, *label3, *combo;
    const gchar *markup = "<b>%s</b>";
    gchar *str1;
    gchar *str2;

    frame = gtk_widget_get_parent (gtk_widget_get_parent (gtk_widget_get_parent ((GtkWidget*)data)));

    gtk_box_reorder_child (GTK_BOX (gtk_widget_get_parent (frame)), frame, 0);

    box = gtk_hbox_new (FALSE, 0);
    gtk_frame_set_label_widget (GTK_FRAME (frame), box);
    gtk_widget_show (frame);

    label1 = gtk_label_new (" ");
    str1 = g_strdup_printf (markup, _("When new mail arri_ves in"));
    gtk_label_set_markup_with_mnemonic (GTK_LABEL (label1), str1);
    g_free (str1);

    label2 = gtk_label_new (" ");

#if GTK_CHECK_VERSION(2, 91, 0)
    combo = gtk_combo_box_text_new ();
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo),
				    n_accounts > 1 ? _("any Inbox") : _("Inbox"));
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), _("any Folder"));
#else
    combo = gtk_combo_box_new_text ();
    gtk_combo_box_append_text (GTK_COMBO_BOX (combo),
                               n_accounts > 1 ? _("any Inbox") : _("Inbox"));
    gtk_combo_box_append_text (GTK_COMBO_BOX (combo), _("any Folder"));
#endif
    gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 
                              only_inbox ? 0 : 1);
    g_signal_connect (combo, "changed", G_CALLBACK (on_combo_changed), NULL);
     
    label3 = gtk_label_new (":");
    /* i18n: ':' is used in the end of the above line (When New Mail Arrives:)*/
    str2 = g_strdup_printf (markup, _(":"));
    gtk_label_set_markup (GTK_LABEL (label3), str2);
    g_free (str2);

    if (gtk_widget_get_default_direction () == GTK_TEXT_DIR_RTL)
    {
      gtk_box_pack_end (GTK_BOX (box), label3, FALSE, FALSE, 0);
      gtk_box_pack_end (GTK_BOX (box), combo, FALSE, FALSE, 0);
      gtk_box_pack_end (GTK_BOX (box), label2, FALSE, FALSE, 0);
      gtk_box_pack_end (GTK_BOX (box), label1, FALSE, FALSE, 0);
    }
    else
    {
      gtk_box_pack_start (GTK_BOX (box), label1, FALSE, FALSE, 0);
      gtk_box_pack_start (GTK_BOX (box), label2, FALSE, FALSE, 0);
      gtk_box_pack_start (GTK_BOX (box), combo, FALSE, FALSE, 0);
      gtk_box_pack_start (GTK_BOX (box), label3, FALSE, FALSE, 0);
    }

    gtk_widget_show_all (box);
  }
  else
  {
    g_debug ("EI: Woops, couldn't find the GtkFrame in the widget hierarchy");  
  }

  vbox = gtk_vbox_new (FALSE, 8);
  gtk_table_attach((GtkTable *)data->parent, vbox, 0, 1, 0, 1, 0, 0, 0, 0);

	check = gtk_check_button_new_with_mnemonic (_("Pla_y a sound"));
  g_object_set (check, "active", play_sound, NULL);
  gtk_box_pack_start (GTK_BOX (vbox), check, FALSE, FALSE, 0);
  g_signal_connect (check, "toggled", G_CALLBACK (on_sound_toggled), NULL);

	check = gtk_check_button_new_with_mnemonic (_("_Display a notification"));
  g_object_set (check, "active", show_bubble, NULL);
  gtk_box_pack_start (GTK_BOX (vbox), check, FALSE, FALSE, 0);
  g_signal_connect (check, "toggled", G_CALLBACK (on_bubble_toggled), NULL);

	check = gtk_check_button_new_with_mnemonic (_("_Indicate new messages in the panel"));
  g_object_set (check, "active", show_count, NULL);
  gtk_box_pack_start (GTK_BOX (vbox), check, FALSE, FALSE, 0);
  g_signal_connect (check, "toggled", G_CALLBACK (on_show_panel_toggled), NULL);

	gtk_widget_show_all (vbox);
	
  return check;
}

/*
 *
 * SHOW EVOLUTION CODE
 *
 */

static void
show_evolution (gpointer arg0, guint timestamp, gpointer arg1)
{
#define MAIL_ICON "evolution-mail"
  EShell *shell = e_shell_get_default ();

  g_debug ("EI: Showing Evolution to user");

  if (shell)
  {
    GSList    *i;
    GList     *list;
    GtkWindow *mail_window = NULL;
    EShellWindow *shell_window;
    EShellView *shell_view;
    GtkAction *action;

    list = e_shell_get_watched_windows (shell);

    /* Find the first EShellWindow in the list. */
    while (list != NULL && !E_IS_SHELL_WINDOW (list->data))
        list = g_list_next (list);

    g_return_if_fail (list != NULL);

    /* Present the shell window. */
    shell_window = E_SHELL_WINDOW (list->data);
    if (!evolution_is_focused ())
    {
        gtk_window_present_with_time (GTK_WINDOW (shell_window), timestamp);
    }

    /* Switch to the mail view. */
    shell_view = e_shell_window_get_shell_view (shell_window, "mail");
    action = e_shell_view_get_action (shell_view);
    gtk_action_activate (action);

    /* Setup the indicators */
    for (i = indicators; i; i = i->next)
    {
      IndicateIndicator *indicator = i->data;

      set_indicator_unread_count (indicator, 0);
      indicate_indicator_set_property (indicator,
                                       INDICATE_INDICATOR_MESSAGES_PROP_ATTENTION,
                                       "false");

      g_debug ("EI: Setting %s to 0 unread messages",
               indicate_indicator_get_property (indicator, "name"));

    }
    message_count = 0;
    update_unity_launcher_count ();
  }
  else
  {
    g_warning ("EI: Cannot show window, no shell");
    return;
  }
}


/*
 *
 *  SHOW/HIDE EVOLUTION IN INDICATOR APPLET
 *
 */

static void
show_evolution_in_indicator_applet (void)
{
  gchar *bpath;

  bpath = g_build_filename (g_get_user_config_dir (),
                            USER_BLACKLIST_DIR,
                            USER_BLACKLIST_FILENAME,
                            NULL);

  if (g_file_test (bpath, G_FILE_TEST_EXISTS))
    {
      GFile *bfile;

      bfile = g_file_new_for_path (bpath);

      if (bfile)
        {
          GError *error = NULL;

          g_file_delete (bfile, NULL, &error);

          if (error)
            {
              g_warning ("EI: Unable to remove blacklist file: %s", error->message);
              g_error_free (error);
            }

          g_object_unref (bfile);
        }
    }

  g_free (bpath);
}

static void
hide_evolution_in_indicator_applet (void)
{
  gchar  *bdir;
  gchar  *bpath;
  GError *error = NULL;

  bdir = g_build_filename (g_get_user_config_dir (),
                           USER_BLACKLIST_DIR,
                           NULL);
  if (!g_file_test (bdir, G_FILE_TEST_IS_DIR))
    {
      GFile *dirfile;

      dirfile = g_file_new_for_path (bdir);
      if (dirfile)
        {
          g_file_make_directory_with_parents (dirfile,
                                              NULL, 
                                              &error);
          if (error)
            {
              g_warning ("EI: Unable to create blacklist directory: %s",
                         error->message);
              g_error_free (error);
              g_object_unref (dirfile);
              g_free (bdir);
              g_free (bpath);
              return;
            }
        }
      else
        {
          g_warning ("EI: Unable to create blacklist directory: Unable to create "
                     "GFile for path %s", bdir);
          g_free (bdir);
          g_free (bpath);
          return;
        }

      g_object_unref (dirfile);
    }
  g_free (bdir);

  bpath = g_build_filename (g_get_user_config_dir (),
                            USER_BLACKLIST_DIR,
                            USER_BLACKLIST_FILENAME,
                            NULL);

  if (g_file_set_contents (bpath,
                           EVOLUTION_DESKTOP_FILE,
                           -1,
                           &error))
    {
      g_debug ("EI: Successfully wrote blacklist file to %s", bpath);
    }
  else
    {
      g_debug ("EI: Unable to write blacklist file to %s: %s",
               bpath,
               error ? error->message : "Unknown");
      if (error)
        g_error_free (error);
    }

  g_free (bpath);
}
